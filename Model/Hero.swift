//
//  Hero.swift
//  SuperHeroSearch
//
//  Created by siyabonga zondo on 2020/09/22.
//

import Foundation

struct Hero {
    let id: String
    let heroName: String
    let imgUrl:[String : String]
}

extension Hero: Decodable{
    enum HeroCodingKeys : String, CodingKey{
        case id
        case heroName = "name"
        case imgUrl = "image"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: HeroCodingKeys.self)
        
        id = try container.decode(String.self, forKey: .id)
        heroName = try container.decode(String.self, forKey: .heroName)
        imgUrl = try container.decode([String : String].self, forKey: .imgUrl)
    }
}
