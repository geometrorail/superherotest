//
//  PowerStats.swift
//  SuperHeroSearch
//
//  Created by siyabonga zondo on 2020/09/22.
//

import Foundation

struct PowerStats {
    let intelligence: String
    let strength: String
    let speed:String
    let durbility: String
    let power: String
    let combat:String
}

extension PowerStats: Decodable{
    enum HeroCodingKeys : String, CodingKey{
        case intelligence
        case strength
        case speed
        case durability
        case power
        case combat
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: HeroCodingKeys.self)
        
        intelligence = try container.decode(String.self, forKey: .intelligence)
        strength = try container.decode(String.self, forKey: .strength)
        speed = try container.decode(String.self, forKey: .speed)
        durbility = try container.decode(String.self, forKey: .durability)
        power = try container.decode(String.self, forKey: .power)
        combat = try container.decode(String.self, forKey: .combat)
    }
}
