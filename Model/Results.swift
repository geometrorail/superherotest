//
//  Results.swift
//  SuperHeroSearch
//
//  Created by siyabonga zondo on 2020/09/22.
//

import Foundation

struct Results {
    let hero : [Hero]
}

extension Results: Decodable {
    
    private enum ResultsCodingKeys: String, CodingKey{
        case hero = "results"
    }
    
    init(from decoder:Decoder) throws {
        let container = try decoder.container(keyedBy: ResultsCodingKeys.self)
        
        hero = try container.decode([Hero].self, forKey: .hero)
    }
}
