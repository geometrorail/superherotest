//
//  HeroService.swift
//  SuperHeroSearch
//
//  Created by siyabonga zondo on 2020/09/22.
//

import Foundation
import Moya

enum HeroApi {
    case SearchHero(SearchText: String)
    case GetHeroById(HeroId:String)
}

extension HeroApi: TargetType{
    
    var baseURL: URL{
        guard let url = URL(string: "https://superheroapi.com/api/") else {
            fatalError("baseUrl exception.")
        }
        return url
    }
    
    var path: String {
        switch self {
        case .SearchHero(let searchText):
            return "\(NetworkManager.HeroToken)/search/\(searchText)"
        case .GetHeroById(let heroId):
            return "\(NetworkManager.HeroToken)/\(heroId)"
        default:
            return "\(NetworkManager.HeroToken)/644" //will return charcter 644 by default
        }
    }
    
    var method: Moya.Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .SearchHero:
            return .requestParameters(parameters: ["api_key" :NetworkManager.HeroToken], encoding: URLEncoding.queryString)

        case .GetHeroById(HeroId: let heroId):
            return .requestParameters(parameters: ["id" :heroId], encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-type" : "application/json"]
    }
}


