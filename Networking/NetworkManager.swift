//
//  NetworkManager.swift
//  SuperHeroSearch
//
//  Created by siyabonga zondo on 2020/09/22.
//

import Foundation
import Moya
import MBProgressHUD

protocol Network {
    associatedtype T: TargetType
    var provider : MoyaProvider<T> { get }
}

struct NetworkManager: Network {
    
    static let HeroToken = "367147230975348"
    let provider = MoyaProvider<HeroApi>(plugins: [NetworkLoggerPlugin(verbose: true)])
    
    func searchSuperHero(searchedText: String, completion: @escaping ([Hero]) ->()){
    
        provider.request(.SearchHero(SearchText: searchedText)){
            result in
            switch result {
            case let .success(response):
                do {
                    let results = try JSONDecoder().decode(Results.self, from: response.data)
                    completion(results.hero)
                    
                }catch let err {
                    print(err)
                }
            case let .failure(error):
                print(error)            
            }
           
        }
    }
    
    func getSuperHero(id: String, completion: @escaping ([Hero]) ->()){
    
        provider.request(.GetHeroById(HeroId: id)){
            result in
            switch result {
            case let .success(response):
                do {
                    let results = try JSONDecoder().decode(Results.self, from: response.data)
                    completion(results.hero)
                    
                }catch let err {
                    print(err)
                }
            case let .failure(error):
                print(error)
            }
           
        }
    }
}
