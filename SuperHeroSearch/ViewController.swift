//
//  ViewController.swift
//  SuperHeroSearch
//
//  Created by siyabonga zondo on 2020/09/22.
//

import UIKit
import MBProgressHUD
import SDWebImage

class ViewController: UIViewController {
    
    // MARK: - Properties
    
    let searchBar = UISearchBar()
    
    var searchedText = ""
    
    var networkProvider: NetworkManager!
    
    let textLabel = UILabel(frame: CGRect(x:0, y:0, width: 200, height: 21))
    let imageView = UIImageView()
    
    let blueThemeColor = UIColor(red: 55/255, green: 120/255,blue: 250/255, alpha: 1)
    
    init(networkProvider: NetworkManager){
        super.init(nibName: nil, bundle: nil)
        self.networkProvider = networkProvider
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        loadDefaultHero()
    }
    
    // MARK: - Selectors
    
    @objc func handleShowSearchBar() {
        searchBar.becomeFirstResponder()
        search(shouldShow: true)
    }

    // MARK: - Helper Functions
    
    func configureUI() {
        view.backgroundColor = .black
        
        searchBar.sizeToFit()
        searchBar.delegate = self
        searchBar.barTintColor = .white
        
        //changes text bar color to white
        let searchTextField = searchBar.value(forKey: "searchField") as? UITextField
        searchTextField?.textColor = UIColor.white
        
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.backgroundColor = .black
    
        navigationItem.title = "Super Hero"
        
        showSearchBarButton(shouldShow: true)
        
        //Image View
        imageView.backgroundColor = UIColor.gray
        imageView.heightAnchor.constraint(equalToConstant: 520.0).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: self.view.frame.width - 40).isActive = true
        imageView.layer.cornerRadius = 10

        //Text Label
        textLabel.textColor = UIColor.white
        textLabel.widthAnchor.constraint(equalToConstant: self.view.frame.width).isActive = true
        textLabel.heightAnchor.constraint(equalToConstant: 20.0).isActive = true
        textLabel.textAlignment = .center

        //Stack View
        let stackView   = UIStackView()
        stackView.axis  = NSLayoutConstraint.Axis.vertical
        stackView.distribution  = UIStackView.Distribution.equalSpacing
        stackView.alignment = UIStackView.Alignment.center
        stackView.spacing   = 16.0

        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(textLabel)
        stackView.translatesAutoresizingMaskIntoConstraints = false

        self.view.addSubview(stackView)

        //Constraints
        stackView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }
    
    func showSearchBarButton(shouldShow: Bool) {
        if shouldShow {
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search,
                                                                target: self,
                                                                action: #selector(handleShowSearchBar))
        } else {
            navigationItem.rightBarButtonItem = nil
        }
    }
    
    func search(shouldShow: Bool) {
        showSearchBarButton(shouldShow: !shouldShow)
        searchBar.showsCancelButton = shouldShow
        navigationItem.titleView = shouldShow ? searchBar : nil
    }
    
    func loadDefaultHero(){
   
        networkProvider.searchSuperHero(searchedText: "Batman"){
            response in
            
            self.navigationItem.title = response[0].heroName
            self.textLabel.text = "Search for other superheroes"
            self.imageView.sd_setImage(with: URL(string: response[0].imgUrl["url"] ?? ""))
    
        }
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print("Search bar editing did begin..")
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print("Search bar editing did end..")
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        search(shouldShow: false)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("Search text is \(searchText)")
        searchedText = searchText
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.showLoadingHUD(text: "Fetching hero...")
        networkProvider.searchSuperHero(searchedText: searchedText){
            response in
            
            self.navigationItem.title = response[0].heroName
            self.textLabel.text = response[0].heroName
            self.imageView.sd_setImage(with: URL(string: response[0].imgUrl["url"] ?? ""))
            self.hideLoadingHUD()
        }
    }
    
    func showLoadingHUD(text:String) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = text
    }

    func hideLoadingHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}

